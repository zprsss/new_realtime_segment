//index.js
//获取应用实例
const app = getApp()
const { $Toast } = require('../../dist/base/index');

Page({
  data: {
    page: 1,
    count: 10,
    result: [],
    refresh: true,
    triggered: false,
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    var that = this;
    wx.request({
      url: 'https://api.apiopen.top/getJoke?page='+this.data.page+'&count='+this.data.count+'&type=video',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        if (res.data.code!=200){
          $Toast({
            content: '列表拉取失败',
            type: 'error'
          });
        }
        that.setData({
          result: res.data.result
        });
        console.dir(that.data.result);
      }
    })
    
  },
  updateList:function(){
    var that = this;
    this.setData({
      page : that.data.page+1
    });
    
    wx.request({
      url: 'https://api.apiopen.top/getJoke?page='+that.data.page+'&count='+that.data.count+'&type=video',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        if (res.data.code!=200){
          $Toast({
            content: res.data.message,
            type: 'error'
          });
          return;
        }
        that.setData({
          result: res.data.result.concat(that.data.result)
        });
      }
    })
    //关闭 下拉刷新
    this.setData({
      triggered:false
    })
  },
  onRefresh() {
    console.log(1)
    if (this._freshing) return
    this._freshing = true
    setTimeout(() => {
      this.setData({
        triggered: false,
      })
      this._freshing = false
    }, 3000)
  },
 
})
